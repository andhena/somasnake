# SomaSnake v1.3
### A robust and interoperable end-to-end analysis pipeline for clonal reconstruction and interpretation of somatic NGS data.
________________________________________________________________________________


[![Anaconda-Server Badge](https://anaconda.org/andhena/somasnake/badges/version.svg)](https://anaconda.org/andhena/somasnake)
[![Anaconda-Server Badge](https://anaconda.org/andhena/somasnake/badges/downloads.svg)](https://anaconda.org/andhena/somasnake)
[![Anaconda-Server Badge](https://anaconda.org/andhena/somasnake/badges/license.svg)](https://anaconda.org/andhena/somasnake) 
[![Anaconda-Server Badge](https://anaconda.org/andhena/somasnake/badges/latest_release_date.svg)](https://anaconda.org/andhena/somasnake) 
[![Anaconda-Server Badge](https://anaconda.org/andhena/somasnake/badges/platforms.svg)](https://anaconda.org/andhena/somasnake)



This README is divided in five distinct parts :

[1. Getting Started](#GS)

* [1.1 Installation of conda](#GS)

* [1.2 Installation of SomaSnake](#IS)

* [1.3 Downloading databases](#DD)

* [1.4 Installation of Control-FREEC](#CF)

* [1.5 Preparation of input nomenclature](#IN)

[2. Usage](#U)

* [2.1 Preparation of the configuration file](#CF)

* [2.2 Run SomaSnake](#RS)

* [2.3 Dry-run](#DR)

* [2.4 Build DAG](#BD)

* [2.5 Advanced Parameters](#AP)

* [2.6 Maftools results interpretation](#MF)

[3. Authors](#AT)

[4. License](#LI)

[5. Acknowledgements and contact information](#AK)

[6. Cite SomaSnake](#HTC)

________________________________________________________________________________

### <a name="GS"></a> 1. Getting Started

#### <a name="GS"></a> 1.1 Installation of conda

In order to work properly SomaSnake requires conda, that you can install using [Miniconda](https://docs.conda.io/en/latest/miniconda.html) with Python 3.

Conda shall be added in your PATH (into your .bashrc or similar).

Be careful and check you have the last version of conda:

```
conda update conda
conda update --all
```

Then you have to check if you have the required channels (bioconda first, then conda-forge) :

```
conda config --show-sources
```

And if it does not contain bioconda first, then defaults and conda-forge, you have to add them:

```
conda config --add channels andhena
conda config --add channels conda-forge
conda config --add channels bioconda
```

#### <a name="IS"></a> 1.2 Installation of SomaSnake

SomaSnake can therefore be installed using conda within a new virtual environment (recommended):
```
#create a new env 
conda create -n mysomasnake somasnake
#activate env
conda activate mysomasnake

```

This can be long, depending of the dependecies that are already installed on your machine.

So if this is the first time you are using conda, it would probably take a while, which is completely normal.

Also, depending on the version of your conda, you can have this message "failed with initial frozen solve. Retrying with flexible solve."

Please have a look at [this](https://github.com/conda/conda/issues/9367) if you are experiencing troubles with the installation.

#### <a name="DD"></a> 1.3 Downloading databases

* ###### <a name="DD"></a> Get human reference fasta file

```
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz

```

* ###### Get distinct chromosome fasta files (used by Control-FREEC)
```
for i in {1..22} ; do wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chr$i.fa.gz ; done
```
```
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chrX.fa.gz
```
```
wget http://hgdownload.soe.ucsc.edu/goldenPath/hg38/chromosomes/chrY.fa.gz
```

* ###### <a name="DB"></a> Get af_gnomAD, dbnsfp, dbsnp and indels databases
```
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/Mutect2/af-only-gnomad.hg38.vcf.gz
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/Mutect2/af-only-gnomad.hg38.vcf.gz.tbi
```
```
wget https://dbnsfp.s3.amazonaws.com/dbNSFP4.8a.zip
```
```
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/dbsnp_146.hg38.vcf.gz
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/dbsnp_146.hg38.vcf.gz.tbi
```
```
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
wget ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi
```

If one of the links is broken, please let me know an I will update the url.

`/!\ EDIT` : The Broad Institute uses Google to download public databases now : [google cloud](https://console.cloud.google.com/storage/browser/genomics-public-data/resources/broad/hg38/v0/)

#### <a name="CF"></a> 1.4 Installation of Control-FREEC

Follow the instructions on the BOEVA lab website: [Control-FREEC](http://boevalab.inf.ethz.ch/FREEC/tutorial.html)

#### <a name="IN"></a> 1.5 Preparation of input nomenclature

SomaSnake is sensitive to input fastq file nomenclature.
Each fastq file must be named like "**sample_type_strand.fastq.gz**"

While "sample" is the **id of your sample**, "type" is whether it is from **tumor or control sample**, and "strand" is the **orientation of DNA strand**.

It is completely up to you to pick them, but it has to be separated by an underscore "_".

Here is an example:

- ***sampleID_tumor_R1.fastq.gz***

- ***sampleID_tumor_R2.fastq.gz***


And in case you have a matched control:

- ***sampleID_germline_R1.fastq.gz***

- ***sampleID_germline_R2.fastq.gz***

This nomenclature is essential for SomaSnake to detect id sample and differenciate sample types.

If a matched control is not available, you must provide a Panel of Normals according to [GATK4 Mutect2 documentation](https://software.broadinstitute.org/gatk/documentation/tooldocs/current/org_broadinstitute_hellbender_tools_walkers_mutect_CreateSomaticPanelOfNormals.php).

### <a name="U"></a> 2. Usage of SomaSnake

#### <a name="CF"></a> 2.1 Preparation of the configuration file

The configuration file is automatically generated using the command :

```
somasnake config    --config CONFIG --sample SAMPLE_ID --ngs NGS --bed BED 
                    --capture-size CAPTURE_SIZE --sex SEX --control CONTROL
                    --fastq FASTQ_DIRECTORY --somatic TUMOR --germline NORMAL
                    --forward R1 --reverse R2  [--pon PANEL_OF_NORMALS]
                    --assembly ASSEMBLY --ref HG_FASTA_SEQUENCE --hg-length HG_GENOME_LENGTH 
                    --chr-fasta-folder CHROMOSOMES_FASTA_FOLDER
                    --dbsnp DBSNP --indels INDELS --dbnsfp DBNSFP --refgenes REFGENES 
                    --gnomad GNOMAD_ALLELE_FREQUENCY --vep VEP_CACHE_DIR
                    --results-dir OUTPUT --email EMAIL --freec FREEC --depth MIN_DEPTH
```

Be careful with the nomenclature of the fastq files (See [1.5](#IN)).

It required the following arguments :

|Options            | Example               | Description                                                       |
|:------------------|----------------------:|:------------------------------------------------------------------|
|--config           |   my-config.yml       |    Name of the configuration file that will be created            |
|--sample           |   patient00           |    ID of the sample to analyse                                    |
|--ngs              |   WES                 |    Type of NGS sequencing (WES or WGS)                            |
|--bed              |   file.bed            |    File containing capture coordinates ([bed format](https://www.ensembl.org/info/website/upload/bed.html)). GATK resources for WGS [here](https://console.cloud.google.com/storage/browser/_details/genomics-public-data/resources/broad/hg38/v0/wgs_calling_regions.hg38.interval_list) and Agilent design for WES [here](https://earray.chem.agilent.com/suredesign/)       |
|--capture-size     |   40218220            |    Size of the capture (used for sequencing) in bp                |
|--sex              |   XX                  |    Sex of the sample (XX or XY)                                   |
|--control          |   TRUE                |    TRUE if a paired control sample is provided, else FALSE        |
|--fastq            |   fastq/dir/          |    Directory containing fastq.gz files for the sample             |
|--somatic          |   tumor               |    Nomenclature for somatic sample                                |
|--germline         |   normal              |    Nomenclature for germline sample, write NA or None if CONTROL=FALSE                               |
|--forward          |   R1                  |    Nomenclature for the forward strand in your fastq.gz file      |
|--reverse          |   R2                  |    Nomenclature for the reverse strand in your fastq.gz file      |
|--pon              |   panel_of_normals.vcf|    COMPULSORY IF CONTROL=FALSE. See [GATK4 Mutect2 documentation](https://software.broadinstitute.org/gatk/documentation/tooldocs/current/org_broadinstitute_hellbender_tools_walkers_mutect_CreateSomaticPanelOfNormals.php)   |
|--assembly         |   GRCh38              |    [Vep assembly](https://www.ensembl.org/info/docs/tools/vep/script/vep_options.html) e.g. GRCh38                                       |
|--ref              |   hg38.fa             |    [Reference genome in fasta format](#DD) (+index)              |
|--hg-length        |   hg38.len            |    [Control-FREEC](http://boevalab.inf.ethz.ch/FREEC/tutorial.html) chrLenFile: file containing the length of each chromosome                          |
|--chr-fasta-folder |   chr/dir/            |    [Control-FREEC](http://boevalab.inf.ethz.ch/FREEC/tutorial.html) chrFiles: directory of all chromosome*.fa |
|--freec        |   /path/freec            |    [Control-FREEC](http://boevalab.inf.ethz.ch/FREEC/tutorial.html) Full path of the executable freec previously installed                          |
|--dbsnp            |   dbsnp.hg38.vcf.gz   |    [Dbsnp database file](#DB) (+index)                                   |
|--indels           |   indels.hg38.vcf.gz  |    [Mills_and_1000G_gold.indels database file](#DB) (+index)             |
|--dbnsfp           |   dbNSFP3.5a_h.txt.gz |    [Dbnsfp database file](#DB) (+index)                                  |
|--refgenes         |   refgenes.txt        |    List of genes and their coordinates (bed format) for SV annotations. Use [ucsc tables](https://genome.ucsc.edu/cgi-bin/hgTables)                 |
|--gnomad           | af-gnomad.hg38.vcf.gz |    [Allele-Frequency GnomAD database file](#DB) (+index)                 |
|--vep              |   /vep-cache          |    Directory of the [VEP cache](https://www.ensembl.org/info/docs/tools/vep/script/vep_cache.html) v99          |
|--results-dir      |   somasnake-results/  |    Existing directory for output files from SomaSnake analyses    |
|--email            |   user@email.com      |    Your email address for [CRAVAT](http://www.cravat.us/CRAVAT/) results                          |
|--depth            |   30      |    The minimum depth to use for SciClone                          |



#### <a name="RS"></a> 2.2 Run SomaSnake

Run the SomaSnake analysis using the command:

```
somasnake run my-config.yml 
```

`The first run (and only this one) can take some time because it will create all required environments for steps to execute.`
`Do not be scared, be patient and enjoy one (or more) cup of coffee in the meantime.`

#### <a name="DR"></a> 2.3 Dry-run

In case you would like to have a look of what will be executed, use the command :

```
somasnake dry-run my-config.yml 
```

#### <a name="BD"></a> 2.4 Build DAG

In case you would like to have a look of the Directed Acyclic Graph (DAG) of what will be executed, use the command :

```
somasnake dag --configfile my-config.yml --output my-dag.svg
```


#### <a name="AP"></a> 2.5 Advanced Parameters

Most of these parameters are configured with default values. 
Consequently, this part is recommended for advanced bioinformaticians and developers only.

#### <a name="SC"></a> 2.5.1 Using the somasnake-config command

```
somasnake config    --config CONFIG --sample SAMPLE_ID --ngs NGS --bed BED 
                    --capture-size CAPTURE_SIZE --sex SEX --control CONTROL
                    --fastq FASTQ_DIRECTORY --somatic TUMOR --germline NORMAL
                    --forward R1 --reverse R2  [--pon PANEL_OF_NORMALS]
                    --assembly ASSEMBLY --ref HG_FASTA_SEQUENCE --hg-length HG_GENOME_LENGTH 
                    --chr-fasta-folder CHROMOSOMES_FASTA_FOLDER
                    --dbsnp DBSNP --indels INDELS --dbnsfp DBNSFP --refgenes REFGENES 
                    --gnomad GNOMAD_ALLELE_FREQUENCY --vep VEP_CACHE_DIR
                    --results-dir OUTPUT --email EMAIL --freec FREEC --depth MIN_DEPTH 
                    [--min-threads MINIMUM_THREADS] [--max-threads MAXIMUM_THREADS]
                    [--paral JAVA_PARALLEL_THREADS] [--gc-java JAVA_GC_PARALLEL]
                    [--maxmem-java JAVA_MAXIMUM_MEMORY] 
                    [--bwa-lab BWA_LABORATORY] [--bwa-plat BWA_PLATFORM]
                    [--sickle-tech SICKLE_TECHNOLOGY] [--sickle-thresh SICKLE_THRESHOLD]
                    [--markdup-str VALIDATION_STRINGENCY] [--read-str READ_VALIDATION_STRINGENCY]
                    [--pms NUMBER_OF_MUTATIONAL_SIGNATURES] [--msi-ref MSI_REFERENCE]
                    [--enrichr-library ENRICHR_LIBRARY] [--max-af-normal MAX_AF_IN_NORMAL]
                    [--min-af-tumor MIN_AF_IN_SOMATIC] [--min-dp-tumor MIN_DP_IN_SOMATIC]
                    [--gene-list GENES_OF_INTEREST] [--regions-interest REGIONS_OF_INTEREST]

```

|Options            | Default                   | Description                                                       |
|:------------------|--------------------------:|:------------------------------------------------------------------|
|--min-thread       |   1                       |    Minimum number of threads to use per rule                      |
|--max-threads      |   2                       |    Maximum number of threads to use per rule                      |
|--paral            |   2                       |    Number of threads to use for JAVA parallelisation              |
|--gc-java          |   1                       |    Number of threads to use for JAVA garbage collector            |
|--maxmem-java      |   4G                      |    Maximum memory for JAVA                                        |
|--bwa-lab          |   genomics                |    For read group header line in [bwa mem](http://bio-bwa.sourceforge.net/bwa.shtml)                          |
|--bwa-plat         |   illumina                |    For read group header line in [bwa mem](http://bio-bwa.sourceforge.net/bwa.shtml)                          |
|--sickle-tech      |   sanger                  |    Sequencing technique for [sickle](http://manpages.ubuntu.com/manpages/bionic/man1/sickle.1.html) parameters                     |
|--sickle-thres     |   30                      |    Quality threshold for [sickle](http://manpages.ubuntu.com/manpages/bionic/man1/sickle.1.html) parameters                        |
|--markdup-str      |   LENIENT                 |    [Gatk MarkDuplicates](https://software.broadinstitute.org/gatk/documentation/tooldocs/4.0.4.0/picard_sam_markduplicates_MarkDuplicates.php) --VALIDATION_STRINGENCY argument           |
|--read-str         |   LENIENT                 |    [Gatk ApplyBQSR](https://software.broadinstitute.org/gatk/documentation/tooldocs/4.beta.2/org_broadinstitute_hellbender_tools_walkers_bqsr_ApplyBQSR.php) --read-validation-stringency argument           |
|--pms              |   3                       |    Number of signatures to look for in tumor sample               |
|--msi-ref          |   Null                    |    Output of [msisensor scan](https://github.com/ding-lab/msisensor) if command previously executed         |
|--enrichr-library  |GO_Biological_Process_2018 |    Library from [EnrichR](https://amp.pharm.mssm.edu/Enrichr/#stats) for functional analysis                  |
|--max-af-normal    |   0.1                     |    Maximum allele frequency found in control sample               |
|--min-af-tumor     |   0.01                    |    Mininum allele frequency found in tumor sample                 |
|--min-dp-tumor     |   10                      |    Minimum read depth of allele found in tumor sample             |
|--gene-list        |   Null                    |    List of genes to highlight in results (file with 1 gene ID per line)|
|--regions-interest |   Null                    |    List of genomic regions to highlight in results (3 columns, [bed format](https://www.ensembl.org/info/website/upload/bed.html))|


#### <a name="SC"></a> 2.5.2 Using the configuration file template

`/!\ FOR DEVELOPERS ONLY`

The SomaSnake conda package contains a configuration template. 
You can copy it (DO NOT REMOVE OR CHANGE IT) and then directly write your parameters into it. 
This template contains comments to help you understand the function of each variables. Then you can use it as you configuration file.

#### <a name="MF"></a> 2.6 Maftools on single sample

SomaSnake generates maf files and figures for variants overview with maftools.
However, the oncoplot and variants overview figures are meaningful only when analysing a cohort, thus it is not recommended to interpret data from the maftools output of one sample.

After running SomaSnake on multiple samples, simply merge the maf files of your cohort (located in module5) in one large maf file.

In case your individual maf files are in the same folder, you can run the following:

```
# Get header and write it in a new file
head -n 1 sample1.filtered.maf > header_maf

# Create temporary maf files with no header
for f in *.filtered.maf; do sed '1d' $f> noheader_$f ; done

# Concatenate the maf header and individual maf files with no header
cat header_maf noheader_* > merged-samples.maf

# remove header file and maf files with no header
# check first that you will not delete anything you do not want to
ls noheader_*
rm header.maf noheader_*
```

Then simply follow the instructions [here](http://bioconductor.org/packages/release/bioc/vignettes/maftools/inst/doc/maftools.html#62_Reading_MAF_files) to generate oncoplots using your new file merged-sample.maf.


### <a name="AT"></a> 3. Authors

* ##### **Charlotte Andrieu** - *Initial and main work* - [andhena](https://github.com/andhena) 

* ###### Swann Meyer - *Contribution* - [SwannM](https://github.com/SwannM) during his master internship.

### <a name="LI"></a> 4. License

This project is licensed under the GPLv3 License - see the LICENSE file for details.

### <a name="AK"></a> 5. Acknowledgments and contact information

SomaSnake started as a part of Charlotte's PhD project about the characterization of the 
genomic profiles of the Circulating Tumor Cells (CTCs) and the identification of 
biomarkers, between 2018 and 2019.
Since then, the pipeline has been maintained yearly, however feel free to contact me at 
andrieu.charlotte@hotmail.fr for any issues.

Contribution of Swann Meyer was made possible thanks to his participation to the ExoCaRE project granted by the PRT-K16-155 2016 provided by the French National Cancer Institute (INCa) during 2019.

You will find the documentation for this package within the doc/ folder of this repository.

Special thanks to [Sacha](https://github.com/dridk) for the conda package bug resolution.


### <a name="HTC"></a> 6. Cite SomaSnake

Andrieu et al., (2019) “_SomaSnake: A robust and interoperable end-to-end analysis pipeline for the interpretation of somatic NGS data_”; Available online at: https://gitlab.com/andhena/somasnake
