#!/usr/bin/python
# Creation : 12/2017
# Author : Charlotte Andrieu

# Libraries
from optparse import OptionParser
import re



# Details of available options : input and output
parser = OptionParser(usage='usage: %prog [options]')

parser.add_option('-i', '--input',
                  dest='input_file',
                  type='string',
                  help='File that will be used, it must be a file with columns 3 and 4 corresponding to REF '
                       'nucleotide and ALT nucleotide, respectively.')
parser.add_option('-o', '--output',
                  dest='output_file',
                  type='string',
                  help='Output txt filename')


(options, args) = parser.parse_args()

# Compulsory options
if not options.input_file:
    parser.error('Input file not given')
if not options.output_file:
    parser.error('Output txt filename not given')

# Variables
a = "A"
t = "T"
c = "C"
g = "G"
all_count = 0
var_count = 0
transversions_count = 0
transitions_count = 0
ctoa = 0
gtot = 0
ctot = 0
gtoa = 0
ctog = 0
gtoc = 0
ttoa = 0
atot = 0
ttoc = 0
atog = 0
ttog = 0
atoc = 0

others = 0

# Open file line by line and count transitions and transversions
inputfile = open(options.input_file)
for line in inputfile.readlines():
    line = line.strip()
    if not line.startswith("#"):
        all_count += 1
        col = line.split("\t")
        ref = col[2]
        alt = col[3]
        if ref == a:
            if alt == g:
                transitions_count += 1
                atog += 1
            elif alt == t:
                transversions_count += 1
                atot += 1
            elif alt == c:
                transversions_count += 1
                atoc += 1
            else:
                others += 1
        if ref == g:
            if alt == a:
                transitions_count += 1
                gtoa += 1
            elif alt == t:
                transversions_count += 1
                gtot += 1
            elif alt == c:
                transversions_count += 1
                gtoc += 1
            else:
                others += 1
        if ref == c:
            if alt == t:
                transitions_count += 1
                ctot += 1
            elif alt == a:
                ctoa += 1
                transversions_count += 1
            elif alt == g:
                ctog += 1
                transversions_count += 1
            else:
                others += 1
        if ref == t:
            if alt == c:
                ttoc += 1
                transitions_count += 1
            elif alt == a:
                ttoa += 1
                transversions_count += 1
            elif alt == g:
                ttog += 1
                transitions_count += 1
            else:
                others += 1
inputfile.close()

# Write results into output file
outputfile = open(options.output_file, "w")
display = str(all_count)+"\ttotal variants identified including :\n"+str(transitions_count)+"\ttransitions\n"+str(transversions_count)+"\ttransversions\n"
display2 = str(ctoa)+"\tC>A\n"+str(ctot)+"\tC>T\n"+str(ctog)+"\tC>G\n"+str(ttoa)+"\tT>A\n"+str(ttoc)+"\tT>C\n"+str(ttog)+"\tT>G\n"
display3 = str(gtot)+"\tG>T\n"+str(gtoa)+"\tG>A\n"+str(gtoc)+"\tG>C\n"+str(atot)+"\tA>T\n"+str(atog)+"\tA>G\n"+str(atoc)+"\tA>C\n"

outputfile.write(display)
outputfile.write(display2)
outputfile.write(display3)
