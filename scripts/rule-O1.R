#!/usr/bin/env Rscript
# Author : Charlotte Andrieu
# Adapted from https://github.com/genome/sciclone 
# 2019

#source("http://bioconductor.org/biocLite.R")
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager", repos = "http://cran.r-project.org")
BiocManager::install(version = "3.16")
if(!require(devtools))BiocManager::install(c("devtools" ))
if(!require(limma))BiocManager::install(c( "limma" ))
if(!require(IRanges))BiocManager::install(c( "IRanges" ))
library(devtools)
options(rgl.useNULL = TRUE)
if(!require(NORMT3))install.packages("https://cran.r-project.org/src/contrib/Archive/NORMT3/NORMT3_1.0.4.tar.gz",repos=NULL, method="libcurl")
if(!require(bmm)) install_github("genome/bmm")
if(!require(sciClone)) install_github("genome/sciclone")
library(sciClone)

args <- commandArgs()
argvcf <- paste(args[4])
argcnv <- paste(args[5])
argout <- paste(args[6])
argpdf <- paste(args[7])
argid <- paste(args[8])
normal <- paste(args[9])
min_depth <- paste(args[10])

# for snp file
vcf = read.table(argvcf, header = TRUE,  sep = "\t", check.names = FALSE, comment.char="")


#if somatic only there is no constit_GT
nb_headernormal=length(grep(normal, colnames(vcf), value = TRUE)) #test if column normal is recovered

if (nb_headernormal>0){ #is the case of pon or normal
    names(vcf)[6:13] <- c("constit_GT", "constit_DP","constit_AD","constit_AF","somatic_GT","somatic_DP","somatic_AD","somatic_AF")
} else{#only somatic
    names(vcf)[6:9] <- c("somatic_GT","somatic_DP","somatic_AD","somatic_AF")
}
head(vcf)
snp_file = subset(vcf, select =c("CHROM", "POS"))
snp_file$count_ref = lapply(strsplit(as.character(vcf$somatic_AD),",",fixed = TRUE),"[",1)
snp_file$count_alt = lapply(strsplit(as.character(vcf$somatic_AD),",",fixed = TRUE),"[",2)
snp_file$VAF <- as.numeric(as.character(vcf$somatic_AF))*100
snp_file$POS <- as.numeric(snp_file$POS)
snp_file$count_ref <- as.numeric(snp_file$count_ref)
snp_file$count_alt <- as.numeric(snp_file$count_alt)
snp_file$VAF <- as.numeric(snp_file$VAF)

# for cnv file
cnv = read.table(argcnv, header = FALSE)
cnv_file = subset(cnv, select =c("V1", "V2", "V3", "V4"))


# #1d clustering on just one sample
sc = sciClone(vafs=snp_file,
              copyNumberCalls=cnv_file,
              sampleNames=argid, minimumDepth=min_depth )
#create output
writeClusterTable(sc, argout)
sc.plot1d(sc,argpdf)

