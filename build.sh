echo "Building package..."

# Transfer files from the local package
mkdir -p $PREFIX/opt/somasnake/
cp -r * $PREFIX/opt/somasnake/

# Add soft link of executables in bin/ to have them in the $PATH
ln -s $PREFIX/opt/somasnake/somasnake $PREFIX/bin/
chmod u+x $PREFIX/bin/somasnake

echo "SomaSnake package has been built !"
